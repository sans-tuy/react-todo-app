import "./App.css";
import React, { useEffect, useState } from "react";
import axios from "axios";

const Blank = () => {
  return (
    <div className="blank-wrapper">
      <p className="text-blank">Create Your First Code! :{")"}</p>
    </div>
  );
};

function App() {
  const [todoItems, settodoItems] = useState("");
  const [items, setitems] = useState([]);
  const [edit, setedit] = useState(false);

  const handleChange = (event) => {
    settodoItems(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const temp = {
      id: null,
      value: "",
    };

    temp["value"] = todoItems;
    axios
      .post("http://localhost:3000/nama", temp)
      .then(() => getData())
      .catch((err) => console.log(err));
    settodoItems("");
  };

  const getData = () => {
    axios
      .get("http://localhost:3000/nama")
      .then((res) => setitems(res.data))
      .catch((err) => console.log(err));
    console.log(items);
  };

  const postData = (event) => {
    event.preventDefault();
    axios
      .post("http://localhost:3000/nama")
      .then((res) => setitems(res.data))
      .catch((err) => console.log(err));
    console.log(items);
  };

  const deleteData = (id) => {
    console.log(id);
    axios
      .delete(`http://localhost:3000/nama/${id}`)
      .then(() => getData())
      .catch((err) => console.log(err));
  };

  const updateData = (todoItems, id) => {
    axios
      .patch(`http://localhost:3000/nama/${id}`, {
        value: todoItems,
      })
      .then(() => getData())
      .catch((err) => console.log(err));
    settodoItems("");
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="container">
      <div className="card">
        <h1 style={{ color: "#1abc9c" }}>TODO APP</h1>
        <div className="input-wrapper">
          <input
            value={todoItems}
            onChange={handleChange}
            className="input"
            placeholder="Task"
          />
          <button onClick={handleSubmit} className="button">
            ADD
          </button>
        </div>
        <div className="gap" />
        <div className="result">
          {items.length == 0 ? <Blank /> : null}
          {items.map((item) => (
            <div className="fill-wrapper" key={item.id}>
              <p hidden={edit === true ? "hidden" : null} className="text-fill">
                {item.value}
              </p>
              <input
                value={todoItems}
                style={{ marginTop: 20, marginBottom: 20 }}
                onChange={handleChange}
                className="input"
                placeholder={item.value}
                hidden={edit === false ? "hidden" : null}
              />
              <div className="fil-button-wrapper">
                <button
                  className="fill-button2"
                  onClick={() => {
                    updateData(todoItems, item.id);
                    setedit(false);
                  }}
                  hidden={edit === false ? "hidden" : null}
                >
                  simpan
                </button>
                <button
                  className="fill-button2"
                  hidden={edit === true ? "hidden" : null}
                  onClick={() => setedit(true)}
                >
                  Edit
                </button>
                )
                <button
                  hidden={edit === true ? "hidden" : null}
                  className="fill-button"
                  onClick={() => deleteData(item.id)}
                >
                  Delete
                </button>
                <button
                  hidden={edit === false ? "hidden" : null}
                  className="fill-button"
                  onClick={() => setedit(false)}
                >
                  Cancel
                </button>
              </div>
            </div>
          ))}
          {/* {items.length == 0 ? (
            <Blank />
          ) : (
            <Fill values={items} hapus={hapus} edit={edit} />
          )} */}
        </div>
      </div>
    </div>
  );
}

export default App;
