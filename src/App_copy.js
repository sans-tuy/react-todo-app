import "./App.css";
import React, { Component, useState } from "react";

const Blank = () => {
  return (
    <div className="blank-wrapper">
      <p className="text-blank">Create Your First Code! :{")"}</p>
    </div>
  );
};

const Fill = (values) => {
  // {
  //   console.log("values:", values);
  // }
  return (
    <div className="fill-wrapper">
      {/* {values.map((value) => (
        <>
          <p className="text-fill">{value} </p>
          <div className="fil-button-wrapper">
            <button className="fill-button2">Edit</button>
            <button className="fill-button">Delete</button>
          </div>
        </>
      ))} */}
    </div>
  );
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      todoItems: "",
      items: [],
    };
  }

  handleChange = (event) => {
    this.setState({
      todoItems: event.target.value,
    });
    console.log(this.todoItems);
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({
      items: [...this.state.items, this.todoItems],
      todoItems: "",
    });
  };

  render() {
    return (
      <div className="container">
        <div className="card">
          <h1 style={{ color: "#1abc9c" }}>TODO APP</h1>
          <div className="input-wrapper">
            <form onSubmit={this.handleSubmit}>
              <input
                value={this.state.todoItems}
                onChange={this.handleChange}
                className="input"
                placeholder="Task"
              />
              <button className="button">ADD</button>
            </form>
          </div>
          <div className="gap" />
          <div className="result">
            {/* {arr.length == 1 ? <Blank /> : <Fill values={arr} />} */}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
